import { Cliente } from './cliente';

export const CLIENTES : Cliente[] = [
    {id:1,nombre:'Jose', apellido:'Arreola', email:'arrejose@algo.com', createAt:'2021-05-27'},
    {id:2,nombre:'Maria', apellido:'Sancehz', email:'mariasa@algo.com', createAt:'2021-05-27'},
    {id:3,nombre:'Rosario', apellido:'Salinas', email:'rosasal@algo.com', createAt:'2021-05-27'},
    {id:4,nombre:'Marisol', apellido:'Gordillo', email:'magordillo@algo.com', createAt:'2021-05-27'},
    {id:5,nombre:'Jaime', apellido:'Rodriguez', email:'rodriguez@algo.com', createAt:'2021-05-27'},
    {id:6,nombre:'Rosendo', apellido:'Jimenez', email:'jimenes@algo.com', createAt:'2021-05-27'}
  ];
